import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {StyleSheet, Alert,Text, View,Button,FlatList,TouchableOpacity,Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import TransferenceFirst from './app/components/transferences/TransferenceFirst'
import TransferenceSecond from './app/components/transferences/TransferenceSecond'
import TransferenceThird from './app/components/transferences/TransferenceThird'


function LogoTitle() {
  return (
    <View style={{ flexDirection:'row',alignItems:'center'}}>
    <Image
      style={{ width: 70, height: 70 ,flexDirection:'column'}}
      source={require('./logo.png')}
    />
    <View style={{left:140,flexDirection:'column',alignItems:'center' }}>
      <Text >App</Text>
      </View>
    </View>
  );
}

function HomeScreen({navigation}){
  return(
    <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
      <Text>
        Home Screen
      </Text>
      <Button title="Go to Details"
      onPress={()=> navigation.navigate('Details')}/>
    </View>
  )
}
function DetailsScreen({navigation}){
      return(
        <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
        <Text>
          Details Screen
        </Text>
        <Button title="Go to Home"
        onPress={()=> navigation.navigate('Home')}/>
      </View>
      );
  }


const TransferenceStack= createStackNavigator();
function TransferenceStackScreen(){
  return(
    <TransferenceStack.Navigator>
      <TransferenceStack.Screen name="First" component={TransferenceFirst}/>
      <TransferenceStack.Screen name="Second" component={TransferenceSecond}/>
      <TransferenceStack.Screen name="Third" component={TransferenceThird}/>
    </TransferenceStack.Navigator>
  )
}

  
const Tab = createMaterialBottomTabNavigator();
const Stack= createStackNavigator();
function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Home" tabBarOptions={{activeTintColor: '#e91e63'}}>
        <Tab.Screen name="Home" component={HomeScreen} options={{
          tabBarLabel:'Home',
          tabBarIcon:({color,size})=>(
            <MaterialCommunityIcons name="home" color={color} size={size}/>
          ),
        }}
        />
        <Tab.Screen name="Transference" component={TransferenceStackScreen} options={{
          tabBarLabel:'Transference',
          tabBarIcon:({color,size})=>(
            <MaterialCommunityIcons name="home" color={color} size={size}/>
          ),
        }}
        />
        <Tab.Screen name="Details"component={DetailsScreen} options={{
          tabBarLabel:'Details',
          tabBarIcon:({color,size})=>(
            <MaterialCommunityIcons name="bell" color={color} size={size}/>
          ),
        }}
        />
      </Tab.Navigator>
    </NavigationContainer>
    /*<NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="navegacion" component={navegacion} options={{headerTitle:props => <LogoTitle/>,headerStyle:{backgroundColor:'red'}}}/>
      </Stack.Navigator>
    </NavigationContainer>*/
  );
}

export default App;

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },
  itemTextContainer:{
    flexDirection: 'column',
    paddingTop: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  itemContainer: {
    flexDirection:'row',
    borderColor:'green',
  },
  countTex: {
    color : '#FF00FF',
  },
  img:{
    width: 150,
    height: 150,
    margin: 10,
    borderRadius: 10,
    borderColor: '#ff00ff00',
  },
  line:{
    height: 2, 
    width: '100%', 
    backgroundColor: '#DADADA',
  },
  itemNameD: {
    fontSize: 24,
    marginLeft: 20,
   },
  itemNameN: {
    fontSize: 17,
    marginLeft: 20,
    paddingTop: 8,
    color: '#BAB2B2',
  },
  itemNameE: {
    fontSize: 20,
    marginLeft: 20,
  },
});