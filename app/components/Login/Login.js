import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View, 
    Image,
    TextInput,
    Button
  } from 'react-native'

class Login extends Component{
    render(){
        return(
            <View style={styles.container}>
         <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://i.pcmag.com/imagery/reviews/01PYkmnFAcQSHlyOgKpbNZu-7.fit_scale.size_1028x578.v_1569483538.jpg',
        }}
      />
         <View style= {{padding:30}}>
         <Text  style={styles.textStyle}>Usuario: </Text>
         <TextInput 
          style= {{width: 400, borderColor: 'gray', borderWidth: 1}}
          placeholder={'Usuario'}
          />
          </View>
          <View style= {{padding:30}}>
           <Text style={styles.textStyle}>Contraseña: </Text>
         <TextInput 
          style= {{width: 400, borderColor: 'gray', borderWidth: 1}}
          secureTextEntry={true}
          placeholder={'Password'}
          />
          <TouchableOpacity
         style={styles.button}
        >
         <Text style={{fontSize:15 , paddingTop:20,color:'blue'}}>Olvidaste tu Contraseña?</Text>
        </TouchableOpacity>
          </View>
          <Button title='Sign in' onPress={()=> this.props.navigation.navigate('navegacion')}/>
          <TouchableOpacity
         style={styles.button}
        >
         <Text style={{fontSize:15 , paddingTop:60,color:'blue'}}>Registrarse ahora</Text>
        </TouchableOpacity>
      </View>
        );
    }
}
export default Login;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      padding: 40,
    },
    button: {
      alignItems: 'center',
      padding: 10,
      width: 400,
      marginBottom: 10,
      borderRadius: 30,
    },
    img: {
     width: 200,
     height: 200,
     margin: 50,
  },
  tinyLogo: {
    width: 100,
    height: 100,
  },
  textStyle: {
    fontSize: 20,
  }
  })

