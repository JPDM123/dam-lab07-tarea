import 'react-native-gesture-handler';
import React,{Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {StyleSheet, Alert,Text, View,Button,FlatList,TouchableOpacity,Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import OurFlatList from '../ourFlatList/OurFlatList'


function Item({title,image,year,descripcion,navigation}){
  return(
    <View>
      <TouchableOpacity onPress={()=> navigation.navigate('Details',{title:title, descripcion:descripcion,image:image})}>
      <View style={styles.itemContainer}>
            <Image source = {{uri:image}} style={styles.img}/>
            <View style={styles.itemTextContainer}>
              <Text style={styles.itemNameE}>{title}</Text>
              <Text>{year}</Text>
            </View>
        <View style={styles.line}/>
        </View>
        </TouchableOpacity>
    </View>
  )
}

class ConexionFet extends Component{
  constructor(props){
      super(props);
      this.state = {
          texValue:0,
          count: 0,
          items:[],
          error:null,
      };
  }
  /*
  showAlert = () => {
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text:'OK', onPress:()=>console.log('OK Pressed')},
      ],
      {cancelable:false},
    );
  };
  */
  async componentDidMount() {
      await fetch('https://yts.mx/api/v2/list_movies.json')
          .then(res => res.json())
          .then(
              result => {
                  console.warn('result',result.data.movies);
                  this.setState({
                      items:result.data.movies,
                  });
              },
              error => {
                  this.setState({
                      error: error,
                  });
              },
          );
  }

  render(){
   return (
      <View style={styles.container}>
          <FlatList
              data={this.state.items.length > 0 ? this.state.items : []}
              renderItem={({item}) => (
                  <Item title={item.title} image={item.medium_cover_image} year={item.year} descripcion={item.description_full} navigation={this.props.navigation}/>
              )}
              keyExtractor={item=>item.id}
          />
          
      </View>
  );
  }
 /*
  render() {
    return (
      <OurFlatList showAlert={this.showAlert}/>
    );
    }
    */
  }

function Detail({route}){
    const {title} = route.params;
    const {descripcion}= route.params;
    const {image} = route.params;
      return(
          <View>
              <View style={styles.text}>
                  <Text> {title} </Text>
              </View>
              <View style={{alignItems:'center', paddingVertical:20}}>
                <Image source={{uri:image}} style= {{height:450,width:400}}/>
              </View>
              <View style={{padding:20}}>
              <Text>{descripcion}</Text>
              </View>
              
          </View>
      );
  }
const Tab = createBottomTabNavigator();
const Stack= createStackNavigator();
  class navegacion extends Component {
    render(){
      return (
          <Tab.Navigator>
            <Tab.Screen name="Home" component={ConexionFet}/>
            <Tab.Screen name="Details" component={Detail}/>
            </Tab.Navigator>  
      );
    } 
}

export default navegacion;

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },
  itemTextContainer:{
    flexDirection: 'column',
    paddingTop: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  itemContainer: {
    flexDirection:'row',
    borderColor:'green',
  },
  countTex: {
    color : '#FF00FF',
  },
  img:{
    width: 150,
    height: 150,
    margin: 10,
    borderRadius: 10,
    borderColor: '#ff00ff00',
  },
  line:{
    height: 2, 
    width: '100%', 
    backgroundColor: '#DADADA',
  },
  itemNameD: {
    fontSize: 24,
    marginLeft: 20,
   },
  itemNameN: {
    fontSize: 17,
    marginLeft: 20,
    paddingTop: 8,
    color: '#BAB2B2',
  },
  itemNameE: {
    fontSize: 20,
    marginLeft: 20,
  },
});