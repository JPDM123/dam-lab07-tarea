import React, { Component } from 'react';

import { View, FlatList, StyleSheet, Text ,Image} from 'react-native';

const Lista = [
  {
    id:'1',
    image: "https://okdiario.com/img/2017/04/13/5-curiosidades-sobre-los-lobos-4.jpg",
    title: "2 lovos viendose fijamente",
  },
  {
    id:'2',
    image: "https://t1.uc.ltmcdn.com/images/3/4/9/img_que_significa_el_lobo_como_animal_de_poder_47943_600_square.jpg",
    title: 'El lovo siempre solitario',
  },
  {
    id:'3',
    image: "https://es.calcuworld.com/wp-content/uploads/sites/2/2019/03/medida-dientes-lobo.jpg",
    title: "Lovo enfocado en la camara.",
  },
];

function Item ({ title,image }) {
    return(
    <View>
      <View>
        <Text style={styles.texto}>{title}</Text>
        </View>
        <Image source = {{uri:image}} style={styles.imagen}/>
    </View>
    );
 }

export default class MyList extends Component {
 render(){
     return (
    <View style={styles.container}>
        <FlatList
            data= {Lista}
            renderItem={({item}) => <Item title={item.title} image={item.image}/>}
            keyExtractor={item=>item.id}
        />
    </View>
 );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    margin:20,
  },
  texto:{
    padding:20,
    alignItems:'center',
    
  },
  imagen:{
    height:160,
    width:160,
  },
});
