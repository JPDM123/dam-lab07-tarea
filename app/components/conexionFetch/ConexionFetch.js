import 'react-native-gesture-handler';
import React,{Component} from 'react';

import {StyleSheet, FlatList, Image, Text, View,TouchableOpacity,Button} from 'react-native';


const DATA=[
    {
        title:'ada',
    },
    {
        title:'adaada',
    }
];

function Item({title,image,navigation}){
    return(
        <View style={styles.item}>
            <TouchableOpacity onPress={()=> navigation.navigate('Details',{title:title})}>
                <Image source={{uri:image}} style= {{height:40,width:40}}/>
                <Text style={styles.title}>{title}</Text>
            </TouchableOpacity>
        </View>
    )

}
function Details({route,navigation}){
    return(
        <View>
            <Text>Estas en Details</Text>
            <Button title='ada' onPress={()=> this.props.navigation.navigate('Home')}/>
        </View>
    );
}

class ConexionFetch extends Component{
    constructor(props){
        super(props);
        this.state = {
            texValue:0,
            count: 0,
            items:[],
            error:null,
        };
    }
    async componentDidMount() {
        await fetch('https://yts.mx/api/v2/list_movies.json')
            .then(res => res.json())
            .then(
                result => {
                    console.warn('result',result.data.movies);
                    this.setState({
                        items:result.data.movies,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

  
    render(){
     return (
        <View style={styles.container}>
            <FlatList
                data={this.state.items.length > 0 ? this.state.items : []}
                renderItem={({item}) => (
                    <Item title={item.title} image={item.small_cover_image} navigation={this.props.navigation}/>
                )}
                keyExtractor={item=>item.id}
            />
            <Button title='GG La life ' onPress={()=> this.props.navigation.navigate('Details')}/>
        </View>
    );
    }
}
export default ConexionFetch;
const styles = StyleSheet.create({
    container:{
        flex:1,

    },
    item:{
        backgroundColor:'gray',
        padding:20,
        marginVertical:10,
        marginHorizontal:16,
    },
    title:{
        fontSize:32,  
    },
});