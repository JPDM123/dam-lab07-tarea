var moment = require('moment');
moment.locale('es');
 
import React, {Component} from 'react';
import {Text, View, Button,  StyleSheet, Alert} from 'react-native';
import { TextInput, Switch } from 'react-native-paper';
 
import ModalSelector from 'react-native-modal-selector';
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class TransferenceFirst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [
      {key: 1, label: "13213412"},
      {key: 2, label: "23425435"},
      {key: 3, label: "31512113"},
      {key: 4, label: "45561642"},
      {key: 5, label: "51213636"},
      {key: 6, label: "61233455"},
    ],
      importe: '',
      cuentaOrigen:'',
      cuentaDestino:'',
      referencia:'',
      isDateTimePickerVisible:false,
      fecha:'Selecciona Fecha',
      fechaFormat:'',
      email:'',
    };
   
  }
 
  showAlert = () => {
    Alert.alert(
      'Complete los campos obligatorios',
      'Campos obligatorios *',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };
 
  showAlert2 = () => {
    Alert.alert(
      'Solo dato numerico',
      'Revise el campo',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };
 
 
  changeImporte = importe => {
    this.setState({importe});
  };
 
  changeReferencia = referencia => {
    this.setState({referencia});
  };
 
  changesEmail = email => {
    this.setState({email});
  };
 
  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };
 
  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };
 
  handleDatePicker = date => {
    console.log('A date has been picked: ', date);
 
    this.setState({
      fecha: moment(date).format('DD/MM/YYYY'),
      fechaFormat: moment(date).format('YY/MM/DD'),
    });
 
    this.hideDateTimePicker();
  };
 
 
 
  render() {
    return (
      <View style={{flex:1, padding:40 }}>
        <KeyboardAwareScrollView>
          <View>
        <View
          style={{
            justifyContent: 'center',
            marginBottom: 5,
          }}>
            <View style = {{flexDirection: 'column', }}>
              <Text style={styles.titleInput}>Cuenta origen *</Text>
              <View style={styles.pickerModel}>
                <ModalSelector
                  data={this.state.accounts}
                  value={this.state.cuentaOrigen}
                  initValue="Todos"
                  cancelText="Cancelar"
                  onChange={(option) => {
                    this.setState({cuentaOrigen:option.label})
                  }}
                  >      
                  </ModalSelector>
              </View>
            </View>
        </View>
 
        <View
          style={{
            justifyContent: 'center',
            marginBottom: 5,
          }}>
            <View style = {{flexDirection: 'column', alignSelf: 'stretch'}}>
              <Text style={styles.titleInput}>Cuenta destino *</Text>
              <View style={styles.pickerModel}>
                <ModalSelector
                  data={this.state.accounts}
                  initValue="Todos"
                  cancelText="Cancelar"
                  value={this.state.cuentaDestino}
                  onChange={(option) => {
                    this.setState({cuentaDestino:option.label})
                  }}
                  />
              </View>
            </View>
        </View>
 
        <View
          style={{
            justifyContent: 'center',
            marginBottom: 5,
          }}>
          <View style={{flexDirection: 'column', alignSelf: 'stretch'}}>
            <Text style={styles.titleInput}>Importe *</Text>
            <View style={[styles.viewInput, {height: 35}]}>
              <TextInput
              underlineColorAndroid={'transparent'}
              style={{height:40, borderRadius: 6}}
              keyboardType='numeric'
              onChangeText={importe => {
                const re = /^[0-9\b]+$/;
                if (importe === '' || re.test(importe)) {
                  this.changeImporte(importe);
                }else{
                  this.showAlert2()
                 
                }
               
              }}
              ></TextInput>
            </View>
          </View>
        </View>
 
        <View
          style={{
            justifyContent: 'center',
            marginBottom: 5,
          }}>
          <View style={{flexDirection: 'column', alignSelf: 'stretch'}}>
            <Text style={styles.titleInput}>Referencia *</Text>
            <View style={[styles.viewInput, {height: 35}]}>
              <TextInput
              underlineColorAndroid={'transparent'}
              style={{height:40, borderRadius: 6}}
              onChangeText={referencia => this.changeReferencia(referencia)}
              />
            </View>
          </View>
        </View>
 
        <View
          style={{
            justifyContent: 'center',
            marginBottom: 5,
            top: 5,
          }}>
          <View style={{alignContent:"center", alignItems:"center"}}>
          <TouchableOpacity onPress={this.showDateTimePicker}>
            <View style = {styles.viewCalendar}>
              <Text Text style={styles.title}>
                {this.state.fecha}
              </Text>
            </View>
          </TouchableOpacity>
          </View>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicker}
            onCancel={this.hideDateTimePicker}
          />
        </View>
 
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            margin: 20,
            alignContent:"center",
          }}>
          <Text
          style={[
            styles.titleInput,
            {marginBottom: 0, marginTop: 2, marginRight: 2},
          ]}>
            Notificarme al e-mail
          </Text>
          <Switch
            onValueChange={email => this.setState({sendMail: email})}
            trackColor={'#063984'}
            thumbColor={'#e2e2e2'}
            trackColor="#cdcdcd"
            value={this.state.sendMail}
          />
        </View>
 
        <Button
          title="Siguiente"
          onPress={() => {
 
            if (this.state.cuentaOrigen != ""
                && this.state.cuentaDestino != ""
                && this.state.referencia != ""
                && this.state.importe != ""
                && this.state.fecha != "Selecciona Fecha"){
              this.props.navigation.navigate('Second',{
                cuentaOrig: this.state.cuentaOrigen,
                cuentaDest: this.state.cuentaDestino,
                impo: this.state.importe,
                ref: this.state.referencia,
                fec: this.state.fecha,
                notificar: this.state.email});
            }else{
              this.showAlert()
            }
          }}
        />
      </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  titleInput: {
    fontSize:15,
 
  },
  viewInput: {
 
  },
  viewCalendar: {
    backgroundColor: "#e1e1e1",
    margin: 10,
    padding:5,
    width: 150,
    height: 30,
    alignItems:"center",
  },
  pickerModel: {
    flex:1,
  },
});