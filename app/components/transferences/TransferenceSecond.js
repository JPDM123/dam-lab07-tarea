var moment = require('moment');
moment.locale('es');
 
import React, {Component} from 'react';
import {Text, View, Button, StyleSheet} from 'react-native';
import { TextInput } from 'react-native-paper';
 
export default class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };
  }
 
 
  Detalle = () => {
 
    const {cuentaOrig} = this.props.route.params;
    const {cuentaDest} = this.props.route.params;
    const {impo} = this.props.route.params;
    const {ref} = this.props.route.params;
    const {fec} = this.props.route.params;
    const {notificar} = this.props.route.params;
   
    return (
      <View style={style.contenedor}>
 
          <Text style={style.titulo}>Cuenta Origen:</Text>
          <TextInput style={style.valor} editable={false}>{cuentaOrig}</TextInput>
          <Text style={style.titulo}>Cuenta Destino:</Text>
          <TextInput style={style.valor} editable={false}>{cuentaDest}</TextInput>
          <Text style={style.titulo}>Importe:</Text>
          <TextInput style={style.valor} editable={false}>{impo}</TextInput>
          <Text style={style.titulo}>Referencia:</Text>
          <TextInput style={style.valor} editable={false}>{ref}</TextInput>
          <Text style={style.titulo}>Fecha:</Text>
          <TextInput style={style.valor} editable={false}>{fec}</TextInput>
         <View style={{ flexDirection:'row', marginTop:20}}>
           <View style={{ paddingLeft:50}}>
          <Button style={style.boton} title="Confirmar" onPress={() => this.props.navigation.navigate('Third')}/>
            </View>
            <View style={{paddingLeft:100}}>
          <Button style={style.boton} title="Volver" onPress={() => this.props.navigation.navigate('First')}/>
          </View>
        </View>
      </View>
    );
  }
 
 
  render() {
    return (
        <this.Detalle/>
    );
  }
}
 
 const style = StyleSheet.create({
  contenedor:{
    padding:8,
    flex:1,
    flexDirection:'column',
    backgroundColor: '#e1e1e1',
    justifyContent: 'flex-start'
  },
  titulo: {
    color: 'black',
    fontSize: 24,
    marginBottom: 5
  },
  valor: {
    fontSize: 25,
  },
  boton: {
    margin:10,
    fontSize:40
  }
});